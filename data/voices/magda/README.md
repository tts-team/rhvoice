# Magda, Polish language voice for RHVoice
This voice is distributed under the [Creative Commons — Attribution 4.0 International — CC BY 4.0 license](https://creativecommons.org/licenses/by/4.0/).

## Team
- Magda Roczeń - Speaker.
- [Roman Roczeń](http://romanro.art.pl) - audio editing.
- [Grzegorz Złotowicz](http://zlotowicz.pl) - voice training, package preparation.
